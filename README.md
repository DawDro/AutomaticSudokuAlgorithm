This code creates random Sudoku game where numbers are different in each square, but random all over the board.

The script prints the board and checks if it's filled correctly according to Sudoku rules.

If there are the same numbers in any row or column the script prints information about it.

This was exercise in Z2J Python learning path to pass exam for the next level.
