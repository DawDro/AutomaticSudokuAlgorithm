# main.py

from random import randint

def create_square():
    square_numbers = []
    while len(square_numbers) < 9:
        rnum = randint(1,9)
        if rnum not in square_numbers:
            square_numbers.append(rnum)
    return square_numbers

def create_board_table():
    all_squares = []
    while len(all_squares) < 9:
        all_squares.append(create_square())
    return all_squares

all_squares = create_board_table()

# Refactored draw_board function divided into 2 functions        
def draw_all_squares(all_squares,from_index_all, to_index_all, from_index, to_index):
    row = []
    for square in all_squares[from_index_all:to_index_all]:
        for num in square[from_index:to_index]:
            row.append(num)
            print(f"|{num}",end="")
        print("|",end="")
    print()
    return row

def draw_board_table():
    lines = []
    print("=====================")
    new_row = draw_all_squares(all_squares,0,3,0,3)
    lines.append(new_row)
    new_row = draw_all_squares(all_squares,0,3,3,6)
    lines.append(new_row)
    new_row = draw_all_squares(all_squares,0,3,6,9)
    lines.append(new_row)
    print("=====================")
    new_row = draw_all_squares(all_squares,2,5,0,3)
    lines.append(new_row)
    new_row = draw_all_squares(all_squares,2,5,3,6)
    lines.append(new_row)
    new_row = draw_all_squares(all_squares,2,5,6,9)
    lines.append(new_row)
    print("=====================")
    new_row = draw_all_squares(all_squares,5,8,0,3)
    lines.append(new_row)
    new_row = draw_all_squares(all_squares,5,8,3,6)
    lines.append(new_row)
    new_row = draw_all_squares(all_squares,5,8,6,9)
    lines.append(new_row)

    col = "columns"
    row = "rows"
    check_line_or_col(lines,row)
    columns_lis = create_columns(lines)
    check_line_or_col(columns_lis,col)

#------- draw_board before refactoring
""" def draw_board(all_squares_lists):
    lines = []
    rows = []
    print("=====================")
    for table in all_squares_lists[0:3]:
        rows.append(table[0])
        rows.append(table[1])
        rows.append(table[2])
        print(f"|{table[0]}|{table[1]}|{table[2]}|",end="")
    print("")
    lines.append(rows)
    rows = []
    for table in all_squares_lists[0:3]:
        rows.append(table[3])
        rows.append(table[4])
        rows.append(table[5])
        print(f"|{table[3]}|{table[4]}|{table[5]}|",end="")
    print("")
    lines.append(rows)
    rows = []
    for table in all_squares_lists[0:3]:
        rows.append(table[6])
        rows.append(table[7])
        rows.append(table[8])
        print(f"|{table[6]}|{table[7]}|{table[8]}|",end="")
    print("")
    lines.append(rows)
    rows = []
    print("=====================")
    for table in all_squares_lists[2:5]:
        rows.append(table[0])
        rows.append(table[1])
        rows.append(table[2])
        print(f"|{table[0]}|{table[1]}|{table[2]}|",end="")
    print("")
    lines.append(rows)
    rows = []

    for table in all_squares_lists[2:5]:
        rows.append(table[3])
        rows.append(table[4])
        rows.append(table[5])
        print(f"|{table[3]}|{table[4]}|{table[5]}|",end="")
    print("")
    lines.append(rows)
    rows = []

    for table in all_squares_lists[2:5]:
        rows.append(table[6])
        rows.append(table[7])
        rows.append(table[8])
        print(f"|{table[6]}|{table[7]}|{table[8]}|",end="")
    print("")
    lines.append(rows)
    rows = []
    print("=====================")
    for table in all_squares_lists[5:8]:
        rows.append(table[0])
        rows.append(table[1])
        rows.append(table[2])
        print(f"|{table[0]}|{table[1]}|{table[2]}|",end="")
    print("")
    lines.append(rows)
    rows = []

    for table in all_squares_lists[5:8]:
        rows.append(table[3])
        rows.append(table[4])
        rows.append(table[5])
        print(f"|{table[3]}|{table[4]}|{table[5]}|",end="")
    print("")
    lines.append(rows)
    rows = []

    for table in all_squares_lists[5:8]:
        rows.append(table[6])
        rows.append(table[7])
        rows.append(table[8])
        print(f"|{table[6]}|{table[7]}|{table[8]}|",end="")
    print("")
    lines.append(rows)
    rows = []
    print("=====================")

    col = "columns"
    row = "rows"
    check_line_or_col(lines,row)
    columns_lis = create_columns(lines)
    check_line_or_col(columns_lis,col) """

    
def check_line_or_col(table_with_lines, col_or_row):
    for line in table_with_lines:
        line_len = len(line)
        act_set = set(line)
        if len(act_set) < line_len:
            return print(f"There are duplicated numbers in {col_or_row}! Solution is not correct.")

def create_columns(table_with_lines):
    columns = []

    for n in range(9): 
        temp_col = []
        for line in table_with_lines:
            temp_col.append(line[n])
        columns.append(temp_col)
    return columns        

# draw_board(all_squares)
draw_board_table()